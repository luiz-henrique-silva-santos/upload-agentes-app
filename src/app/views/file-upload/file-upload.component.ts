import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { FileUploadService } from 'src/app/services/file-upload.service';



@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.sass']
})
export class FileUploadComponent implements OnInit {
  private subscription: Subscription = new Subscription();
  currentFile: File[] = [];
  uploadFiles: File[] = [];
  progress = 0;
  message = '';


  fileName = 'Select File';

  ngOnInit(): void {

  }

  constructor(private uploadService: FileUploadService) { }

  selectFile(event: any): void {
    this.fileName = "";
    var fileNames: String[] = []
    for (var i = 0; i < event.target.files.length; i++) {
      const file: File = event.target.files[i];
      this.uploadFiles.push(file);
      fileNames.push(file.name)
      this.fileName = fileNames.toString();
    }
  }

  upload(): void {
    this.currentFile = this.uploadFiles;
  }

//-- Caso o upload deva aguardar o anterior terminar para iniciar o próximo
 // upload(): void {
 //  // 
 //   this.progress = 0;
 //   this.message = "";
 //   this.loader = "true";
 //   this.uploadFiles.forEach(file => {
 //     
 //     if (this.currentFile) {
 //       this.subscription.add(
 //         this.uploadService.upload(file).subscribe({
 //           next: this.handleSuccess.bind(this),
 //           error: this.handleError.bind(this)
 //         })
 //       )
 //     }
 //   })
 //   this.loader = "false";
 // }
//
 // handleError(err: any) {
 //   console.log(err);
 //   this.progress = 0;
//
 //   if (err.error && err.error.message) {
 //     this.message = err.error.message;
 //   } else {
 //     this.message = 'Could not upload the file!';
 //   }
 // }
//
 // handleSuccess(event: any) {
 //   if (event.type === HttpEventType.UploadProgress) {
 //     this.progress = Math.round(100 * event.loaded / event.total);
 //   } else if (event instanceof HttpResponse) {
 //     this.message = event.body.message;
 //   }
 // }

}

