import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { RegiaoResponseDTO } from 'src/app/models/regiao.model';
import { FileUploadService } from 'src/app/services/file-upload.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.sass']
})
export class ReportsComponent {
  private subscription: Subscription = new Subscription();
  public dataSource = new MatTableDataSource<RegiaoResponseDTO>();
  displayedColumns: string[] = ['sigla', 'lista_valores_geracao', 'qtd_geracao', 'lista_valores_compra', 'qtd_compra'];
  filterValue: string = "";

  constructor(private uploadService: FileUploadService) { }

  ngOnInit(): void {
    this.getReportAll();
  }

  getReportFiltered(): void {
    if (this.filterValue != "") {
      this.subscription.add(
        this.uploadService.getRegiaoReportFiltered(this.filterValue.toUpperCase()).subscribe({
          next: this.handleSuccess.bind(this),
          error: this.handleError.bind(this)
        })
      )
    } else {
      this.getReportAll();
    }
  }

  getReportAll(): void {
    this.subscription.add(
      this.uploadService.getRegiaoReport().subscribe({
        next: this.handleSuccess.bind(this),
        error: this.handleError.bind(this)
      })
    )
  }

  handleError(err: any) {
    console.log(err);

  }

  handleSuccess(resp: any) {
    this.dataSource.data = resp;
  }
}
