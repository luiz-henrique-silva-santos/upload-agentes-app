import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RegiaoResponseDTO } from '../models/regiao.model';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  private baseUrl = 'http://localhost:8080/api/v1/teste-api';

  constructor(private http: HttpClient) { }

  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.baseUrl}/upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }

  getRegiaoReport(): Observable<RegiaoResponseDTO[]> {
    return this.http.get<RegiaoResponseDTO[]>(`${this.baseUrl}/regiao/report`);
  }

  getRegiaoReportFiltered(filter: string): Observable<RegiaoResponseDTO[]> {
    return this.http.get<RegiaoResponseDTO[]>(`${this.baseUrl}/regiao/${filter}/report`);
  }
}