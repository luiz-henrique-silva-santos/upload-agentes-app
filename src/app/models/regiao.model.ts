import { Model } from "./model";

export class RegiaoResponseDTO extends Model {
    public sigla: string;
    public lista_valores_geracao: Array<String>
    public qtd_geracao: string;
    public lista_valores_compra: Array<String>
    public qtd_ompra: string;

    constructor(object?: any) {
        super(object)
    }

}