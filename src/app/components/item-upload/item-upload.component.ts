import { Component, Input } from '@angular/core';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-item-upload',
  templateUrl: './item-upload.component.html',
  styleUrls: ['./item-upload.component.sass']
})
export class ItemUploadComponent {
  _data?: File;
  fileName = "";
  componentType = '';
  progress = 0;
  message = '';
  currentFile?: File;
  loader = "false";

  private subscription: Subscription = new Subscription();

  @Input() set data(data: File) {
    this.currentFile = data;
    this.fileName = data.name;
    this.componentType = data.type as string;
  }

  ngOnInit(): void {
    this.upload();
  }

  constructor(private uploadService: FileUploadService) { }

  upload(): void {
    this.progress = 0;
    this.message = "";
    this.loader = "true"; 
    if (this.currentFile) {
      this.subscription.add(
        this.uploadService.upload(this.currentFile).subscribe({
          next: this.handleSuccess.bind(this),
          error: this.handleError.bind(this)
        })
      )
    }
  }

  handleError(err: any) {
    console.log(err);
    this.progress = 0;

    if (err.error && err.error.message) {
      this.message = err.error.message;
    } else {
      this.message = 'Could not upload the file!';
    }

    this.currentFile = undefined;
    this.loader = "false"; 
  }

  handleSuccess(event: any) {
    if (event.type === HttpEventType.UploadProgress) {
      this.progress = Math.round(100 * event.loaded / event.total);
    } else if (event instanceof HttpResponse) {
      this.message = event.body.message;
      this.loader = "false"; 
    }
  }

}
